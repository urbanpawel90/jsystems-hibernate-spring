package com.jsystems.hbmspring;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SanityUnitTest {
  @Test
  public void test_sanity() {
    assertEquals(2 + 2, 4);
  }
}
