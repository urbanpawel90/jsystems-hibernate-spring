<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ss" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Welcome ${name}!</h1>
<h3>Config list</h3>
<p>${message}</p>
<table>
    <c:forEach items="${config}" var="item">
        <tr>
            <td>${item.key}</td>
            <td>${item.value}</td>
            <ss:authorize access="isAuthenticated() and !hasRole('ADMIN')">
                <td>Delete (only for admin)</td>
            </ss:authorize>
            <ss:authorize access="hasRole('ADMIN')">
                <td><a href="remove/${item.key}">Delete</a></td>
            </ss:authorize>
        </tr>
    </c:forEach>
</table>

</body>
</html>
