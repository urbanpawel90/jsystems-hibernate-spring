<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<sf:form method="POST" modelAttribute="config">
    <sf:input path="key"/>
    <sf:input path="value"/>
    <input type="submit" value="Save"/>
</sf:form>

</body>
</html>
