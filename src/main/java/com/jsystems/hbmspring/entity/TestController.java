package com.jsystems.hbmspring.entity;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

@Controller
public class TestController {
  private final EmployeeService employeeService;
  private final NewsFeedDataSource newsFeed;

  public TestController(EmployeeService employeeService,
      /* @Qualifier("rest-news") */ NewsFeedDataSource newsFeedDataSource) {
    this.employeeService = employeeService;
    this.newsFeed = newsFeedDataSource;
  }

  // http://localhost:8082/jsystems/user/purban

  @GetMapping("/user/{username}")
  @ResponseBody
  public String displayUserDetails(
      @PathVariable("username") String username,
      @RequestParam(value = "email",
          required = false,
          defaultValue = "false") boolean showEmail,
      @RequestParam(value = "date")
      @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
    return String.format("Hello %s! email=%s, date=%s", username,
        Boolean.toString(showEmail), date);
  }

  @GetMapping("/test")
  @ResponseBody
  public List<Employee> testEnv() {
    newsFeed.getNews();
    return employeeService.getEmployees();
  }
}
