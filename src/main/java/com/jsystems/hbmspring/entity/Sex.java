package com.jsystems.hbmspring.entity;

public enum Sex {
  MAN("Male"), WOMAN("Female");

  private final String description;

  Sex(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return description;
  }
}
