package com.jsystems.hbmspring.entity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.util.Date;

public class H2EmployeesDemo {
  public static void main(String[] args) {
    EntityManagerFactory entityManagerFactory = Persistence
        .createEntityManagerFactory("hibernate-h2");
    EntityManager entityManager = entityManagerFactory.createEntityManager();

    // Start transakcji
    entityManager.getTransaction().begin();
    Employee e1 = new Employee();
    e1.setFirstName("Jan");
    e1.setLastName("Kowalski");
    e1.setSalary(new BigDecimal(2000));
    e1.setEmail("test@test.pl");
    e1.setHireDate(new Date());
    e1.setPhoneNumber("111222333");

    Employee e2 = new Employee();
    e2.setFirstName("Arkadiusz");
    e2.setLastName("Nowak");
    e2.setSalary(new BigDecimal(3500));
    e2.setEmail("test2@test.pl");
    e2.setHireDate(new Date());
    e2.setPhoneNumber("111322353");

    try {
      entityManager.persist(e1);
      entityManager.persist(e2);

      // Commit transakcji
      entityManager.getTransaction().commit();
    } catch (Throwable ex) {
      if (entityManager.getTransaction().isActive()) {
        entityManager.getTransaction().rollback();
      }
    } finally {
      entityManager.close();
      entityManagerFactory.close();
    }
  }
}