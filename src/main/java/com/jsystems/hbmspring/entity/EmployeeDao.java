package com.jsystems.hbmspring.entity;

import javax.persistence.EntityManager;
import java.util.List;

public class EmployeeDao {
  private final EntityManager entityManager;

  public EmployeeDao(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  public List<Employee> getAll() {
    return entityManager
        .createQuery("from Employee", Employee.class)
        .getResultList();
  }

  public Employee findById(int id) {
    return entityManager.find(Employee.class, id);
  }

  /**
   * Create or update
   */
  public void save(Employee emp) {
    Employee persistentEmployee = entityManager.merge(emp);
    emp.setId(persistentEmployee.getId());
  }
}
