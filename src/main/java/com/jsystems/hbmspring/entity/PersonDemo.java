package com.jsystems.hbmspring.entity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class PersonDemo {
  public static void main(String[] args) {
    EntityManagerFactory emf = Persistence
        .createEntityManagerFactory("hibernate-h2");
    EntityManager em = emf.createEntityManager();

    Person person = new Person("Maciej", "Nowak", Sex.MAN);
    person.addTag("maciej");
    person.addTag("h2");
    Person person1 = new Person("Ewa", "Kowalska", Sex.WOMAN);
    person1.addTag("woman");
    person1.addTag("ewa");
    person1.addTag("java");

    em.getTransaction().begin();

    em.persist(person);
    em.persist(person1);

    em.getTransaction().commit();

    em.getTransaction().begin();

    person = em.find(Person.class, 2);
    person.getTags().set(1, "EWA_REPLACED");

    person = em.merge(person);

    em.getTransaction().commit();

    List<Person> people = em
        .createQuery("from Person", Person.class)
        .getResultList();

    people.forEach(p -> {
      System.out.println(p);
      System.out.println("\t" + p.getTags());
    });

    em.close();
    emf.close();
  }
}
