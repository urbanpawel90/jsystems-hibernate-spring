package com.jsystems.hbmspring.entity;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportsDao {
  private final EntityManager entityManager;

  public ReportsDao(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  public Map<Employee, BigDecimal> getManagersEmployeesAverageSalary() {
    List<EmployeeAverageSalaryRow> salaries = entityManager
        .createQuery(
            "SELECT new com.jsystems.hbmspring.entity.EmployeeAverageSalaryRow(emp, " +
                "(SELECT avg(sub.salary) FROM emp.subordinates sub)) " +
                "from Employee AS emp " +
                "WHERE emp.subordinates IS NOT EMPTY",
            EmployeeAverageSalaryRow.class)
        .getResultList();

    // Sklejenie salaries i managers w Map<Employee, BigDecimal>
    Map<Employee, BigDecimal> result = new HashMap<>();
    for (EmployeeAverageSalaryRow salaryInfo : salaries) {
      result.put(salaryInfo.getEmployee(), salaryInfo.getAverageSalary());
    }

    System.out.println(result);
    return result;
  }
}
