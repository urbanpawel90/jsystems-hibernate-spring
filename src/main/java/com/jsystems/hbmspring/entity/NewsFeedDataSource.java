package com.jsystems.hbmspring.entity;

import java.util.List;

public interface NewsFeedDataSource {
  List<String> getNews();
}
