package com.jsystems.hbmspring.entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class HibernateGetEmployeesDemo {
  public static void main(String[] args) {
    SessionFactory sessionFactory = new Configuration()
        .configure()
        .buildSessionFactory();
    Session session = sessionFactory.openSession();

    List result = session
        .createQuery("from Employee")
        .list();

    System.out.println(result);

    session.close();
    sessionFactory.close();
  }
}
