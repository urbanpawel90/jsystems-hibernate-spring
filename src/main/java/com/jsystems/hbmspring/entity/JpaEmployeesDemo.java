package com.jsystems.hbmspring.entity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class JpaEmployeesDemo {
  public static void main(String[] args) {
    EntityManagerFactory entityManagerFactory = Persistence
        .createEntityManagerFactory("hibernate-oracle");
    EntityManager entityManager = entityManagerFactory
        .createEntityManager();

    List<Employee> result = entityManager
        .createQuery("from Employee", Employee.class)
        .getResultList();

    System.out.println(result);
    
    entityManager.close();
    entityManagerFactory.close();
  }
}
