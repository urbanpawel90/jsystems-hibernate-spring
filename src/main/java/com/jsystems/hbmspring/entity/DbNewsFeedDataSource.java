package com.jsystems.hbmspring.entity;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Primary
public class DbNewsFeedDataSource implements NewsFeedDataSource {
  @Override
  public List<String> getNews() {
    System.out.println("DbNewsDataSource.getNews()");
    return null;
  }
}
