package com.jsystems.hbmspring.entity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.util.Date;

public class EmployeeDaoDemo {
  public static void main(String[] args) {
    EntityManagerFactory oracleEmf = Persistence.createEntityManagerFactory("hibernate-oracle");
    EntityManagerFactory h2Emf = Persistence.createEntityManagerFactory("hibernate-h2");

    EntityManager oracleEntityManager = oracleEmf.createEntityManager();
    EntityManager h2EntityManager = h2Emf.createEntityManager();

    EmployeeDao oracleDao = new EmployeeDao(oracleEntityManager);
    EmployeeDao h2Dao = new EmployeeDao(h2EntityManager);

    // TODO: Odczyty na Oracle
    System.out.println("== Lista pracownikow: ");
    oracleDao.getAll().forEach(System.out::println);

    System.out.println("== Pracownik ID=99");
    System.out.println(oracleDao.findById(99));

    System.out.println("== Pracownik ID=105");
    Employee employee105 = oracleDao.findById(105);
    System.out.println(employee105);

    System.out.println("== Departament pracownika ID=105");
    System.out.println(employee105.getDepartment());

    // TODO: Dodawanie na H2
    System.out.println("== Nowy pracownik");
    Employee emp = new Employee();
    emp.setFirstName("Jan");
    emp.setLastName("Brodzik");
    emp.setEmail("jbrodzik@wp.pl");
    emp.setPhoneNumber("123456789");
    emp.setHireDate(new Date());
    emp.setSalary(BigDecimal.valueOf(5000));

    h2EntityManager.getTransaction().begin();

    h2Dao.save(emp);
    System.out.println("Nowe ID: " + emp.getId());
    System.out.println(emp);

    h2EntityManager.getTransaction().commit();

    oracleEntityManager.close();
    h2EntityManager.close();

    oracleEmf.close();
    h2Emf.close();
  }
}
