package com.jsystems.hbmspring.entity;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

@Controller
@RequestMapping("/employees")
public class EmployeeController {
  @GetMapping("/test")
  @ResponseBody
  public List<Employee> test() {
    EntityManagerFactory emf =
        Persistence.createEntityManagerFactory("hibernate-oracle");
    EntityManager entityManager = emf.createEntityManager();
    try {
      List<Employee> employees = entityManager
          .createQuery("from Employee e JOIN FETCH e.subordinates",
              Employee.class)
          .getResultList();
      return employees;
    } finally {
      entityManager.close();
      emf.close();
    }
  }
}
