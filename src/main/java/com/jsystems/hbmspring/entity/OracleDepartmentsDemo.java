package com.jsystems.hbmspring.entity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class OracleDepartmentsDemo {
  public static void main(String[] args) {
    EntityManagerFactory entityManagerFactory = Persistence
        .createEntityManagerFactory("hibernate-oracle");
    EntityManager entityManager = entityManagerFactory
        .createEntityManager();

    listEmployeesDepartment(entityManager);
    listDepartmentsEmployees(entityManager);

    listAllDepartmentsWithEmployees(entityManager);

    entityManager.close();
    entityManagerFactory.close();
  }

  private static void listAllDepartmentsWithEmployees(EntityManager entityManager) {
    System.out.println("=============================");
    List<Department> departments = entityManager
        .createQuery("from Department d JOIN FETCH d.employees", Department.class)
        .getResultList();

    for (Department dep : departments) {
      System.out.println(dep);
      dep.getEmployees()
          .forEach(emp -> System.out.println("\t" + emp));
    }
  }

  private static void listDepartmentsEmployees(EntityManager entityManager) {
    Department department = entityManager.find(Department.class, 40);

    System.out.println(department);
    System.out.println(department.getEmployees());
  }

  private static void listEmployeesDepartment(EntityManager entityManager) {
    //    Employee employee = entityManager.find(Employee.class, 105);
    Employee employee = entityManager
        .createQuery("from Employee e WHERE e.id = :id", Employee.class)
        .setParameter("id", 105)
        .getSingleResult();

    System.out.println(employee);
    System.out.println(employee.getDepartment());
  }
}
