package com.jsystems.hbmspring.entity.task.config;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/config-ui")
public class ConfigJspController {
  /*
  Utwórz kontroler, którego metody będą zmapowane do ścieżki /config. Wewnątrz dodaj poniższe metody. Konfiguracja niech będzie obiektem typu Map<>, którym kontroler będzie zarządzał.
GET /config/set - przyjmuje jako query parameter klucz (key) i wartość (value) do wpisania do konfiguracji. Jako wynik użytkownik ma zobaczyć napis OK i informację czy pod danym kluczem był już jakiś element.
GET /config/remove - przyjmuje jako query parameter klucz (key), który należy usunąć z konfiguracji. Jako wynik użytkownik ma zobaczyć wartość, która była pod tym kluczem. Jeżeli wartości nie było należy zwrócić błąd HTTP 404 oraz wyświetlić komunikat o braku klucza w konfiguracji.
GET /config/list - wyświetla na ekranie listę w formie tabelki z obecną konfiguracją.
   */
  private final Map<String, String> config = new HashMap<>();

  @PostConstruct
  private void addExampleData() {
    config.put("url", "jdbc");
    config.put("name", "Pawel");
    config.put("password", "oracle");
  }

  @GetMapping("/create")
  public String showCreateForm(Model model) {
    model.addAttribute("config", new ConfigElement());
    return "add-config";
  }

  @PostMapping("/create")
  public String handleCreateForm(
      @ModelAttribute("config") ConfigElement element,
      RedirectAttributes redirectAttributes) {
    config.put(element.getKey(), element.getValue());
    redirectAttributes.addFlashAttribute("message", "Element created!");
    return "redirect:/config-ui/list";
  }

  @GetMapping("/list")
  public String listConfig(Model model,
      @RequestParam(value = "name",
          required = false,
          defaultValue = "Zenek") String name) {
    model.addAttribute("name", name);
    model.addAttribute("config", config);
    return "list";
  }

  @GetMapping("/remove/{key}")
  public String removeKey(
      @PathVariable("key") String key,
      RedirectAttributes redirectAttributes) {
    String oldValue = config.remove(key);
    redirectAttributes.addFlashAttribute("message", "Element removed!");
    return "redirect:/config-ui/list";
  }

  public static class ConfigElement {
    private String key;
    private String value;

    public String getKey() {
      return key;
    }

    public void setKey(String key) {
      this.key = key;
    }

    public String getValue() {
      return value;
    }

    public void setValue(String value) {
      this.value = value;
    }
  }
}
