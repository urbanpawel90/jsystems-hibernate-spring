package com.jsystems.hbmspring.entity.task.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/config")
public class ConfigController {
  /*
  Utwórz kontroler, którego metody będą zmapowane do ścieżki /config. Wewnątrz dodaj poniższe metody. Konfiguracja niech będzie obiektem typu Map<>, którym kontroler będzie zarządzał.
GET /config/set - przyjmuje jako query parameter klucz (key) i wartość (value) do wpisania do konfiguracji. Jako wynik użytkownik ma zobaczyć napis OK i informację czy pod danym kluczem był już jakiś element.
GET /config/remove - przyjmuje jako query parameter klucz (key), który należy usunąć z konfiguracji. Jako wynik użytkownik ma zobaczyć wartość, która była pod tym kluczem. Jeżeli wartości nie było należy zwrócić błąd HTTP 404 oraz wyświetlić komunikat o braku klucza w konfiguracji.
GET /config/list - wyświetla na ekranie listę w formie tabelki z obecną konfiguracją.
   */
  private final Map<String, String> config = new HashMap<>();

  @GetMapping("/set")
  @ResponseBody
  public String setConfig(
      @RequestParam("key") String key,
      @RequestParam("value") String value) {
    String oldValue = config.put(key, value);
    return "OK, oldValue=" + oldValue;
  }

  @GetMapping("/list")
  @ResponseBody
  public Map<String, String> listConfig() {
    return config;
  }

  @GetMapping("/remove/{key}")
  @ResponseBody
  public ResponseEntity<String> removeKey(
      @PathVariable("key") String key) {
    String oldValue = config.remove(key);
    if (oldValue == null) {
      return ResponseEntity
          .status(HttpStatus.NOT_FOUND)
          .body("Nie znaleziono klucza!");
    }
    return ResponseEntity.ok("oldValue=" + oldValue);
  }
}
