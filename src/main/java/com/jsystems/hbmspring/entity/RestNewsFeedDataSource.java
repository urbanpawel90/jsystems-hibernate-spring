package com.jsystems.hbmspring.entity;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Profile("rest")
public class RestNewsFeedDataSource implements NewsFeedDataSource {
  @Override
  public List<String> getNews() {
    System.out.println("RestNewsFeedDataSource.getNews()");
    return null;
  }
}
