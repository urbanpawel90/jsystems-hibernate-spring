package com.jsystems.hbmspring.entity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ReportsDaoDemo {
  public static void main(String[] args) {
    EntityManagerFactory entityManagerFactory = Persistence
        .createEntityManagerFactory("hibernate-oracle");
    EntityManager entityManager = entityManagerFactory
        .createEntityManager();

    ReportsDao dao = new ReportsDao(entityManager);
    dao.getManagersEmployeesAverageSalary();

    entityManager.close();
    entityManagerFactory.close();
  }
}
