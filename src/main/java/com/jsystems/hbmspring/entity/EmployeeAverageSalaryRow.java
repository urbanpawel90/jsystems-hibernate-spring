package com.jsystems.hbmspring.entity;

import java.math.BigDecimal;

public class EmployeeAverageSalaryRow {
  private final Employee employee;
  private final BigDecimal averageSalary;

  public EmployeeAverageSalaryRow(Employee employee, double averageSalary) {
    this.employee = employee;
    this.averageSalary = BigDecimal.valueOf(averageSalary);
  }

  public Employee getEmployee() {
    return employee;
  }

  public BigDecimal getAverageSalary() {
    return averageSalary;
  }
}
