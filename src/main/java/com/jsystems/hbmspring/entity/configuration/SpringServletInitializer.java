package com.jsystems.hbmspring.entity.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

@EnableWebMvc
@ComponentScan(basePackages = "com.jsystems.hbmspring")
public class SpringServletInitializer
    extends WebMvcConfigurerAdapter
    implements WebApplicationInitializer {

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/img/**").addResourceLocations("/WEB-INF/img/");
  }

  @Override
  public void onStartup(ServletContext servletContext)
      throws ServletException {
    AnnotationConfigWebApplicationContext context =
        new AnnotationConfigWebApplicationContext();
    context.setServletContext(servletContext);
    context.register(SpringServletInitializer.class);
    context.refresh();

    servletContext.addFilter("springSecurityFilterChain",
        new DelegatingFilterProxy("springSecurityFilterChain", context))
        .addMappingForUrlPatterns(null, false, "/*");

    ServletRegistration.Dynamic registration = servletContext
        .addServlet("jsystems", new DispatcherServlet(context));
    registration.addMapping("/");
    registration.setLoadOnStartup(1);
  }
}
