package com.jsystems.hbmspring.entity.configuration;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;

public class RequestLoggingInterceptor extends HandlerInterceptorAdapter {
  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    Logger.getLogger("RequestLogger")
        .info(request.getMethod() + " " +
            request.getRequestURI() + " " +
            request.getRemoteHost());
    return true;
  }
}
